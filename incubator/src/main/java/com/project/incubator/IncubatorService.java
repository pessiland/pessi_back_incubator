package com.project.incubator;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

@EnableScheduling
@Service
public class IncubatorService {
    private final AmqpTemplate rabbitTemplate;
    private final RedisTemplate<String, Set<String>> redisIncubation;
    private final String inventoryServiceLocation = "http://localhost:8084/inventory";
    private final RestTemplate restTemplate;

    public IncubatorService(@Qualifier("redisIncubation")RedisTemplate<String, Set<String>> redisIncubation, AmqpTemplate rabbitTemplate, RestTemplate restTemplate) {
        this.redisIncubation = redisIncubation;
        this.rabbitTemplate = rabbitTemplate;
        this.restTemplate = restTemplate;
    }

    @Async
    public void startIncubationAsync(int playerId) {
        try {
            String useIncubatorUrl = inventoryServiceLocation + "/incubator/use";

            HttpHeaders headers = new HttpHeaders();
            headers.set("playerId", String.valueOf(playerId));

            HttpEntity<String> requestUseIncubator = new HttpEntity<>(null, headers);

            boolean freeIncubator = restTemplate.postForObject(useIncubatorUrl, requestUseIncubator, Boolean.class);

            if (freeIncubator) {
                int incubationTime = generateRandomIncubationTime();
                System.out.println("Incubation time: " + incubationTime + " minutes");

                LocalDateTime currentTime = LocalDateTime.now();
                LocalDateTime endTime = currentTime.plusMinutes(incubationTime);

                String value = String.valueOf(endTime);

                Set<String> hatchingTime = redisIncubation.opsForValue().get(String.valueOf(playerId));
                if (hatchingTime == null) {
                    hatchingTime = new HashSet<>();
                }

                hatchingTime.add(value);

                redisIncubation.opsForValue().set(String.valueOf(playerId), hatchingTime);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    @Scheduled(fixedRate = 1000)
    public void TestIncubationTime() {
        for (String playerId : Objects.requireNonNull(redisIncubation.keys("*"))) {
            Set<String> incubationTimeStr = redisIncubation.opsForValue().get(String.valueOf(playerId));
            if (incubationTimeStr != null) {
                Iterator<String> iterator = incubationTimeStr.iterator();
                while (iterator.hasNext()) {
                    String hatchingTime = iterator.next();
                    LocalDateTime incubationTime = LocalDateTime.parse(hatchingTime);
                    LocalDateTime currentTime = LocalDateTime.now();
                    long durationInMillis = Duration.between(currentTime, incubationTime).toMillis();

                    if (durationInMillis <= 0) {
                        System.out.println("Okey ça part");
                        rabbitTemplate.convertAndSend("spessimen-exchange", "incubation-queue", playerId);
                        iterator.remove();

                        String unlockIncubatorUrl = inventoryServiceLocation + "/incubator/unlock";

                        HttpHeaders headers = new HttpHeaders();
                        headers.set("playerId", String.valueOf(playerId));

                        HttpEntity<String> requestUnlockIncubator = new HttpEntity<>(null, headers);

                        restTemplate.postForObject(unlockIncubatorUrl, requestUnlockIncubator, Void.class);
                    }
                }
                if (incubationTimeStr.isEmpty()) {
                    redisIncubation.delete(playerId);
                } else {
                    redisIncubation.opsForValue().set(playerId, incubationTimeStr);
                }
            }
        }
    }



    private int generateRandomIncubationTime() {
        return new Random().nextInt(10) + 1;
    }
}
