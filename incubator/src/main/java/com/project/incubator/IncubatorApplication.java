package com.project.incubator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableAsync
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class IncubatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(IncubatorApplication.class, args);
    }

}
