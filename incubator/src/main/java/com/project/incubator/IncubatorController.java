package com.project.incubator;

import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/incubator")
public class IncubatorController {

    private final IncubatorService incubatorService;

    public IncubatorController(IncubatorService incubatorService) {
        this.incubatorService = incubatorService;
    }

    @PostMapping("/incubate")
    public void startIncubation(@RequestHeader int playerId) {
        incubatorService.startIncubationAsync(playerId);
    }
}
